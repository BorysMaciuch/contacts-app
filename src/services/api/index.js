import axios from "axios";

export const getContacts = () => {
  try {
    return axios
      .get(
        //temporary solution - would be moved to backend
        "https://cors-anywhere.herokuapp.com/https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
      )
      .then((res) => res.data);
  } catch (e) {
    console.log(e);
  }
};
