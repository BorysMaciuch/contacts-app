import React from "react";
import Contacts from "./pages/contactsPage";

const App = () => {
  return (
    <div className="App">
      <Contacts />
    </div>
  );
};

export default App;
