import React, { useState, useEffect } from "react";
import { getContacts } from "../../services/api";
import ContactsList from "../../components/Contacts";
import SearchBox from "../../components/Search/index";
import { v4 as uuidv4 } from "uuid";
import Header from "../../components/Header";

const Contacts = () => {
  const [contacts, setContacts] = useState([]);
  const [filteredContacts, setFilteredContacts] = useState(contacts);
  const handleGetContacts = async () => {
    const contacts = await getContacts();
    contacts.forEach((contact) => {
      contact.id = uuidv4();
      contact.checked = false;
    });
    setContacts(contacts);
  };
  useEffect(() => {
    handleGetContacts();
  }, []);

  useEffect(() => {
    setFilteredContacts(contacts);
  }, [contacts]);

  const filterContacts = (phrase) => {
    const filteredContacts = [...contacts].filter(
      (contact) =>
        contact.first_name.toLowerCase().includes(phrase.toLowerCase()) ||
        contact.last_name.toLowerCase().includes(phrase.toLowerCase())
    );
    setFilteredContacts(filteredContacts);
  };

  const handleCheckbox = (id) => {
    const contact = filteredContacts.find((contact) => contact.id === id);
    if (contact.checked === false) {
      contact.checked = true;
    } else {
      contact.checked = false;
    }
    const selectedContacts = filteredContacts.filter(
      (contact) => contact.checked === true
    );
    selectedContacts.forEach((contact) => console.log(contact.id));
    setFilteredContacts([...filteredContacts]);
  };
  return (
    <div>
      <Header />
      <SearchBox filterContacts={filterContacts} />
      <ContactsList
        contacts={filteredContacts}
        handleCheckbox={handleCheckbox}
      />
    </div>
  );
};

export default Contacts;
