import React from "react";
import { Container, Typography } from "./styled";
import Avatar from "../Avatar/index";

const Contact = ({ contact, handleCheckbox }) => {
  const { avatar, first_name, last_name, checked, id } = contact;

  const initials = first_name.slice(0, 1) + last_name.slice(0, 1);

  return (
    <Container id={id} onClick={() => handleCheckbox(id)}>
      <Avatar src={avatar} initials={initials} />

      <Typography>{`${first_name} ${last_name}`}</Typography>
      <input type="checkbox" checked={checked} />
    </Container>
  );
};
export default Contact;
