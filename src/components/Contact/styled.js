import styled from 'styled-components'

export const Container = styled.div`
    display:flex;
    width: 100%;
    align-items: center;
    min-height: 60px;
    padding: 10px;
    border-bottom: 1px solid #e0e0eb;
`
export const Typography = styled.div`
    font-size: 20px;
    font-weight: bold;
    padding: 10px;
`


