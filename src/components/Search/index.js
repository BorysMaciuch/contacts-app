import React, { useState } from "react";
import { Input } from "./styled";

const SearchBox = ({ filterContacts }) => {
  const [value, setValue] = useState("");

  const handleOnChange = (e) => {
    console.log("change");
    setValue(e.target.value);
    filterContacts(e.target.value);
  };

  return <Input value={value} onChange={handleOnChange}></Input>;
};

export default SearchBox;
