import styled from "styled-components";

export const Input = styled.input`
  width: 100%;
  outline: none;
  border: none;
  padding: 8px;
  box-sizing: border-box;
  transition: 0.3s;
  &:focus {
    border-color: dodgerBlue;
    box-shadow: 0 0 8px 0 dodgerBlue;
  }
`;
