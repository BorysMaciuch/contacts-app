import React from "react";
import Contact from "../Contact";
import { Container } from "../Contacts/styled";

const ContactsList = ({ contacts, handleCheckbox }) => {
  return (
    <>
      {contacts && (
        <Container>
          {contacts
            .sort((a, b) => (a.last_name > b.last_name ? 1 : -1))
            .map((contact) => (
              <Contact
                handleCheckbox={handleCheckbox}
                contact={contact}
              ></Contact>
            ))}
        </Container>
      )}
    </>
  );
};

export default ContactsList;
