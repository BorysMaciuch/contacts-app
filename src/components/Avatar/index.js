import React from "react";
import { AvatarStyled } from "./styled";

const Avatar = ({ src, initials }) => {
  return (
    <>
      {src ? (
        <AvatarStyled>
          <img height="35px" width="35px" alt="avatar" src={src} />
        </AvatarStyled>
      ) : (
        <AvatarStyled>
          <div>{initials}</div>
        </AvatarStyled>
      )}
    </>
  );
};

export default Avatar;
