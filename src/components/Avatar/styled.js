import styled from "styled-components";

export const AvatarStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  border: 1px solid black;
  min-height: 60px;
  min-width: 60px;
`;
