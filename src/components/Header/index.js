import React from "react";
import { HeaderStyled } from "./styled";

const Header = () => {
  return (
    <HeaderStyled>
      <h2>Contacts</h2>
    </HeaderStyled>
  );
};

export default Header;
