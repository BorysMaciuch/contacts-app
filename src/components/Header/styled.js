import styled from "styled-components";

export const HeaderStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgb(52, 186, 171);
  background: linear-gradient(
    90deg,
    rgba(52, 186, 171, 1) 35%,
    rgba(53, 147, 80, 1) 100%
  );
  color: white;
`;
