# Contacts app

Contacts app is displaying contacts, let you filter them by name and toggle them to have their ID's console log to you. Cool!

## Start locally

To start application you need to run npm install command first:

`npm install`

And then run npm start:

`npm start`

Open http://localhost:3000 to view it in the browser.

## Technologies 

Most important technologies used in project are React, axios, styled-components. State is being managed using built in functionalities of React.